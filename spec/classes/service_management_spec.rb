# frozen_string_literal: true

require 'spec_helper'

describe 'service_management' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do 
        {
          'wanted_pkgs'   => ['ntp'],
          'unwanted_pkgs' => ['less'],
          'disabled_svcs' => ['ntpd'],
        }
        end
      it { is_expected.to compile.with_all_deps }

      it { is_expected.to contain_package('ntp').with(ensure: 'latest') }
    end
  end
end
