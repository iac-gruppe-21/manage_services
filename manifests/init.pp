#
# Installs and uninstalls packages and disables services 
# based on user made lists. 
# @example
#   class {service_management}
#     wanted_packages   => ['ntp'],
#     unwanted_packages => ['rsync'],
#     disabled_services => ['telnet'],
class service_management(
  Tuple $wanted_pkgs,
  Tuple $unwanted_pkgs,
  Tuple $disabled_svcs,
) {

# Sets package provicer to chocolatey if windows.
  if $::operatingsystem == 'windows' {
    Package { provider => chocolatey,
    }
  }

# Installs packages if there are any. 
  if $wanted_pkgs[0] != undef {
    package { $wanted_pkgs:
      ensure => latest,
    }
  }
# Uninstalls packages if there are any.  
  if $unwanted_pkgs[0] != undef {
    package { $unwanted_pkgs:
      ensure => absent,
    }
  }
# Disables services if there are any. 
  if $disabled_svcs[0] != undef {
    Service { $disabled_svcs :
      ensure => stopped,
      enable => false,
    }
  }
}
