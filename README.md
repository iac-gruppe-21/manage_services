# service_management

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with service_management](#setup)
    * [What service_management affects](#what-service_management-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with service_management](#beginning-with-service_management)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

Manages install and uninstall of packages using tuples. It also disables unwanted services running on your system.

## Setup
Depenant on Chocolatey if used on Windows. 

### Beginning with service_management
Include "class {service_management} and specify which packages and services you want as shown in "Usage".
The parameters are of type tuple. 


## Usage

   class {service_management:
     wanted_packages   => ['ntp'],
     unwanted_packages => ['rsync'],
     disabled_services => ['telnet'],
   }